
data "scaleway_object_bucket" "runner_bucket" {
  provider = scaleway.scaleway_development
  name     = "devops-gitlab-runner-cache"
  region   = "fr-par"
}

module "gitlab-runner" {
  source  = "gitlab.com/vigigloo/tf-modules/k8sgitlabrunner"
  version = "0.2.6"

  # @vigigloo source "helm+https://charts.gitlab.io#name=gitlab-runner" "^0.56"
  chart_version = "0.56.0"
  kubeconfig    = data.scaleway_k8s_cluster.dev.kubeconfig[0]
  project_slug  = var.project_slug
  project_name  = var.project_name
  gitlab_groups = [data.gitlab_group.group.group_id]

  cache_provider         = "s3"
  cache_s3_host          = "s3.fr-par.scw.cloud"
  cache_s3_bucket-name   = data.scaleway_object_bucket.runner_bucket.name
  cache_s3_bucket-region = data.scaleway_object_bucket.runner_bucket.region
  cache_s3_access-key    = var.scaleway_cluster_development_access_key
  cache_s3_secret-key    = var.scaleway_cluster_development_secret_key
}
