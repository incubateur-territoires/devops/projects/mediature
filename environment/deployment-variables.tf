module "configure-repository-for-deployment" {
  source  = "gitlab.com/vigigloo/tf-modules/gitlabk8svariables"
  version = "0.1.0"

  repositories = var.repositories
  scope        = var.gitlab_environment_scope

  cluster_user           = module.namespace.user
  cluster_token          = module.namespace.token
  cluster_namespace      = module.namespace.namespace
  cluster_host           = var.kubeconfig.host
  cluster_ca_certificate = base64decode(var.kubeconfig.cluster_ca_certificate)

  base-domain = var.base_domain
}

resource "gitlab_project_variable" "mediature_helm_values" {
  project           = var.mediature_project_id
  environment_scope = var.gitlab_environment_scope
  variable_type     = "file"
  protected         = false

  key   = "HELM_UPGRADE_VALUES"
  value = <<-EOT
  resources:
    limits:
      cpu: 500m
      memory: 1024Mi
    requests:
      cpu: 10m
      memory: 160Mi

  ingress:
    enabled: true
    path: /api/
    annotations:
      kubernetes.io/ingress.class: haproxy

  service:
    targetPort: 3000

  podAnnotations:
    monitoring-org-id: ${var.monitoring_org_id}

  probes:
    liveness:
      tcpSocket:
        port: 3000
    readiness:
      tcpSocket:
        port: 3000

  monitoring:
    exporter:
      enabled: true
    dashboard:
      deploy: true
  EOT
}
