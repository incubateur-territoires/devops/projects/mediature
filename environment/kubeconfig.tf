locals {
  environment-slug = var.gitlab_environment_scope == "*" ? "review" : var.gitlab_environment_scope
}

module "kubeconfig_thomas_rame" {
  source  = "gitlab.com/vigigloo/tf-modules/generatescopedkubeconfig"
  version = "0.3.1"

  filename               = "mediature-thomas-${local.environment-slug}.yml"
  namespace              = module.namespace.namespace
  username               = "thomas-rame"
  project_name           = var.project_name
  role_name              = module.namespace.role_name
  cluster_host           = var.kubeconfig.host
  cluster_ca_certificate = var.kubeconfig.cluster_ca_certificate
}
