# Random generation for secret org id
resource "random_string" "production_secret_org_id" {
  special = false
  length  = 63
}
resource "random_string" "development_secret_org_id" {
  special = false
  length  = 63
}

# Add loki datasource
resource "grafana_data_source" "production_loki" {
  provider = grafana.production
  name     = "loki"
  type     = "loki"
  url      = var.grafana_production_config.loki_url

  is_default = true
  http_headers = {
    X-Scope-OrgID = random_string.production_secret_org_id.result
  }
}

# Add loki datasource
resource "grafana_data_source" "development_loki" {
  provider = grafana.development
  name     = "loki"
  type     = "loki"
  url      = var.grafana_development_config.loki_url

  is_default = true
  http_headers = {
    X-Scope-OrgID = random_string.development_secret_org_id.result
  }
}

resource "grafana_data_source" "production_prometheus" {
  provider = grafana.production
  name     = "mimir"
  type     = "prometheus"
  url      = var.grafana_production_config.prometheus_url

  http_headers = {
    X-Scope-OrgID = random_string.production_secret_org_id.result
  }
}

resource "grafana_data_source" "development_prometheus" {
  provider = grafana.development
  name     = "mimir"
  type     = "prometheus"
  url      = var.grafana_development_config.prometheus_url

  http_headers = {
    X-Scope-OrgID = random_string.development_secret_org_id.result
  }
}
