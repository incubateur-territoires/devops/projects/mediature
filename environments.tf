locals {
  repositories = [
    data.gitlab_project.mediature.id
  ]
}

module "development" {
  source                   = "./environment"
  gitlab_environment_scope = "development"
  kubeconfig               = data.scaleway_k8s_cluster.dev.kubeconfig[0]
  base_domain              = var.dev_base_domain
  namespace                = "${var.project_slug}-development"
  repositories             = local.repositories
  mediature_project_id     = data.gitlab_project.mediature.id
  project_slug             = var.project_slug
  project_name             = var.project_name

  namespace_quota_max_cpu    = 2
  namespace_quota_max_memory = "12Gi"

  monitoring_org_id = random_string.development_secret_org_id.result
}

module "production" {
  source                   = "./environment"
  gitlab_environment_scope = "production"
  kubeconfig               = data.scaleway_k8s_cluster.prod.kubeconfig[0]
  base_domain              = var.prod_base_domain
  namespace                = var.project_slug
  repositories             = local.repositories
  mediature_project_id     = data.gitlab_project.mediature.id
  project_slug             = var.project_slug
  project_name             = var.project_name

  namespace_quota_max_cpu    = 2
  namespace_quota_max_memory = "12Gi"

  monitoring_org_id = random_string.production_secret_org_id.result
}
