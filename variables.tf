# tflint-ignore: terraform_unused_declarations
variable "scaleway_organization_id" {
  type = string
}

variable "dev_base_domain" {
  type = string
}

variable "prod_base_domain" {
  type = string
}

variable "project_slug" {
  type = string
}

variable "project_name" {
  type = string
}

variable "scaleway_cluster_development_project_id" {
  type = string
}
variable "scaleway_cluster_development_access_key" {
  type = string
}
variable "scaleway_cluster_development_secret_key" {
  type      = string
  sensitive = true
}
variable "scaleway_cluster_development_cluster_id" {
  type = string
}
variable "scaleway_cluster_production_project_id" {
  type = string
}
variable "scaleway_cluster_production_access_key" {
  type = string
}
variable "scaleway_cluster_production_secret_key" {
  type      = string
  sensitive = true
}
variable "scaleway_cluster_production_cluster_id" {
  type = string
}

variable "gitlab_token" {
  type = string
}

variable "grafana_development_config" {
  type = object({
    url            = string
    api_key        = string
    org_id         = string
    loki_url       = string
    prometheus_url = string
  })
  sensitive = true
}

variable "grafana_production_config" {
  type = object({
    url            = string
    api_key        = string
    org_id         = string
    loki_url       = string
    prometheus_url = string
  })
  sensitive = true
}
